package com.orioninc.combplanrequestvalidationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CombplanRequestValidationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CombplanRequestValidationServiceApplication.class, args);
	}

}
